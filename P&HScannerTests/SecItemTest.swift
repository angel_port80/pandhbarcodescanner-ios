//
//  SecItemTest.swift
//  BarcodeScanner
//
//  Created by Ángel David Macho Esperilla on 02/03/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import XCTest
@testable import BarcodeScanner

class SecItemTest: XCTestCase {
    
    override func setUp() {
        
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
        
    }
    
    func testAdd() {
        
        let testKey = "testKey"
        
        let testValue = "testValue"
        SecItem.sharedInstance.add(testKey, value: testValue)
        
        var res = SecItem.sharedInstance.find(testKey)
        XCTAssertNotNil(res, "Any value found")
        var resString = String(data: res!, encoding: String.Encoding.utf8)
        XCTAssertEqual(resString, testValue, "Value returned is not the same")
        
        // The new value has to replace the old value
        let newTestValue = "testValueNew"
        SecItem.sharedInstance.add(testKey, value: newTestValue)
        
        res = SecItem.sharedInstance.find(testKey)
        XCTAssertNotNil(res, "Any value found")
        resString = String(data: res!, encoding: String.Encoding.utf8)
        XCTAssertEqual(resString, newTestValue, "Value returned is not the same")
        
    }
    
    func testUpdate() {
        
        let testKey = "testKey"
        
        let testValue = "testValue"
        SecItem.sharedInstance.add(testKey, value: testValue)
        
        // The new value has to replace the old value
        let updatedTestValue = "testValueUpdated"
        SecItem.sharedInstance.update(testKey, newData: updatedTestValue)
        
        let res = SecItem.sharedInstance.find(testKey)
        XCTAssertNotNil(res, "Any value found")
        let resString = String(data: res!, encoding: String.Encoding.utf8)
        XCTAssertEqual(resString, updatedTestValue, "Value returned is not the same")
        
    }
    
    func testDelete() {
        
        let testKey = "testKeyy"
        
        let testValue = "testValue"
        SecItem.sharedInstance.add(testKey, value: testValue)
        
        var res = SecItem.sharedInstance.find(testKey)
        XCTAssertNotNil(res, "Any value found")
        let resString = String(data: res!, encoding: String.Encoding.utf8)
        XCTAssertEqual(resString, testValue, "Value returned is not the same")
        
        // Deletes the key and value
        SecItem.sharedInstance.delete(testKey)
        
        res = SecItem.sharedInstance.find(testKey)
        XCTAssertNil(res, "Key and value still exist")
        
    }
    
    func testFind() {
        
        let testKey = "testKey"
        
        let testValue = "testValue"
        SecItem.sharedInstance.add(testKey, value: testValue)
        
        let res = SecItem.sharedInstance.find(testKey)
        XCTAssertNotNil(res, "Any value found")
        let resString = String(data: res!, encoding: String.Encoding.utf8)
        XCTAssertEqual(resString, testValue, "Value returned is not the same")
        
    }
    
    func testUpdateSafely() {
        
        let testKey = "testKey"
        
        let testValue = "testValue"
        SecItem.sharedInstance.add(testKey, value: testValue)
        
        // The new value has to replace the old value
        let updatedTestValue = "testValueUpdated"
        
        let testNonExistentKey = "testNonExistentKey"
        var isUpdated = SecItem.sharedInstance.updateSafely(testNonExistentKey, newData: updatedTestValue)
        XCTAssertFalse(isUpdated, "Updated a non existent key")
        
        isUpdated = SecItem.sharedInstance.updateSafely(testKey, newData: updatedTestValue)
        XCTAssertTrue(isUpdated, "Did not update an existent key")
        
        let res = SecItem.sharedInstance.find(testKey)
        XCTAssertNotNil(res, "Any value found")
        let resString = String(data: res!, encoding: String.Encoding.utf8)
        XCTAssertEqual(resString, updatedTestValue, "Value returned is not the same")
        
    }
    
    func testDeleteSafely() {
        
        let testKey = "testKey"
        
        let testValue = "testValue"
        SecItem.sharedInstance.add(testKey, value: testValue)
        
        let testNonExistentKey = "testNonExistentKey"
        var isUpdated = SecItem.sharedInstance.deleteSafely(testNonExistentKey)
        XCTAssertFalse(isUpdated, "Deleted a non existent key")
        
        isUpdated = SecItem.sharedInstance.deleteSafely(testKey)
        XCTAssertTrue(isUpdated, "Did not delete an existent key")
        
        let res = SecItem.sharedInstance.find(testKey)
        XCTAssertNil(res, "Key and value still exist")
        
    }

}
