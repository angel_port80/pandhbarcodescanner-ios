//
//  DAOTest.swift
//  BarcodeScanner
//
//  Created by Ángel David Macho Esperilla on 02/03/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import XCTest
@testable import BarcodeScanner

class DAOTest: XCTestCase {
    
    override func setUp() {
        
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
        
    }
    
    func testSave() {
        
        let entityName = "Product"
        let data: [String : AnyObject?] = ["barcode" : "00000000" as Optional<AnyObject>, "name" : "product 1" as Optional<AnyObject>, "photo" : "url photo" as Optional<AnyObject>]
        
        DAO.sharedInstance.save(entityName, data: data)
        
        let argumentArray = ["barcode", "00000000", "name", "product 1", "photo", "url photo"]
        let res = DAO.sharedInstance.fetchWithPredicate(entityName, argumentArray: argumentArray)
        
        let barcode = argumentArray[1]
        let resBarcode = res[0].value(forKey: "barcode") as! String
        let name = argumentArray[3]
        let resName = res[0].value(forKey: "name") as! String
        let photo = argumentArray[5]
        let resPhoto = res[0].value(forKey: "photo") as! String
        XCTAssertEqual(resBarcode, barcode, "It is not the same product. The barcode \(resBarcode) is not equal as \(barcode)")
        XCTAssertEqual(resName, name, "It is not the same product. The name \(resName) is not equal as \(name)")
        XCTAssertEqual(resPhoto, photo, "It is not the same product. The photo \(resPhoto) is not equal as \(photo)")
        
        DAO.sharedInstance.delete(res)
        
    }
    
    func testSaveIfNotExist() {
        
        let entityName = "Product"
        
        var argumentArray = ["barcode", "00000000", "name", "product 1", "photo", "url photo"]
        var res = DAO.sharedInstance.fetchWithPredicate(entityName, argumentArray: argumentArray)
        let count = res.count
        
        let data: [String : AnyObject?] = ["barcode" : "00000000" as Optional<AnyObject>, "name" : "product 1" as Optional<AnyObject>, "photo" : "url photo" as Optional<AnyObject>]
        DAO.sharedInstance.saveIfNotExist(entityName, data: data)
        
        argumentArray = ["barcode", "00000000", "name", "product 1", "photo", "url photo"]
        res = DAO.sharedInstance.fetchWithPredicate(entityName, argumentArray: argumentArray)
        
        if count == 0 {
            XCTAssertEqual(1, res.count)
        } else {
            XCTAssertEqual(count, res.count, "There are \(res.count) same objects and must be \(count)")
        }
        
        DAO.sharedInstance.delete(res)
        
    }
    
    func testUpdate() {
        
        let entityName = "Product"
        let data: [String : AnyObject?] = ["barcode" : "00000000" as Optional<AnyObject>, "name" : "product 1" as Optional<AnyObject>, "photo" : "url photo" as Optional<AnyObject>]
        DAO.sharedInstance.saveIfNotExist(entityName, data: data)
        
        var argumentArray = ["barcode", "00000000", "name", "product 1", "photo", "url photo"]
        var res = DAO.sharedInstance.fetchWithPredicate(entityName, argumentArray: argumentArray)
        var name = argumentArray[3]
        var resName = res[0].value(forKey: "name") as! String
        XCTAssertEqual(resName, name, "It is not the same product. The name \(resName) is not equal as \(name)")
        
        let propertiesToUpdate = ["name" : "product 2"]
        argumentArray = ["barcode", "00000000", "name", "product 1", "photo", "url photo"]
        DAO.sharedInstance.update(entityName, propertiesToUpdate: propertiesToUpdate, argumentArray: argumentArray)
        
        argumentArray = ["barcode", "00000000", "name", "product 2", "photo", "url photo"]
        res = DAO.sharedInstance.fetchWithPredicate(entityName, argumentArray: argumentArray)
        name = argumentArray[3]
        resName = res[0].value(forKey: "name") as! String
        XCTAssertEqual(resName, name, "It is not the same product. The name \(resName) is not equal as \(name)")

        DAO.sharedInstance.delete(res)
        
    }
    
}
