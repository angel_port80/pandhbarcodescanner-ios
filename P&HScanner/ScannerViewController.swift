//
//  ScannerViewController.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 01/12/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

/// Manages Scanner view. Implements barcode scanning functionality.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    // MARK: Properties
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var exitButton: UIButton!
    
    /// Manages the data flow from the capture stage through our input devices.
    var captureSession: AVCaptureSession!
    
    /// Dispays the data as captured from our input device.
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    // MARK: Methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        self.captureSession = AVCaptureSession()
        
        // This object represents a physical capture device and the properties associated with that device
        let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        // Is useful for capturing the data from the input device
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            self.failed();
            return
        }
        
        if (self.captureSession.canAddInput(videoInput)) {
            self.captureSession.addInput(videoInput)
        } else {
            self.failed();
            return;
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (self.captureSession.canAddOutput(metadataOutput)) {
            self.captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypePDF417Code,
                AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code,
                AVMetadataObjectTypeCode39Mod43Code, AVMetadataObjectTypeCode93Code,
                AVMetadataObjectTypeCode128Code]
        } else {
            self.failed()
            return
        }
        
        // Adds the preview layer to display the captured data. Sets the videoGravity to AspectFill so that it covers the full screen
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession);
        self.previewLayer.frame = self.view.layer.bounds;
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        self.view.layer.addSublayer(self.previewLayer)
        self.captureSession.startRunning();
        
        // Bring navigation bar to the front of all other views
        self.view.bringSubview(toFront: self.navBar)
        self.view.bringSubview(toFront: self.exitButton)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        self.previewLayer?.frame = self.view.layer.bounds;
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if (self.captureSession.isRunning == false) {
            self.captureSession.startRunning();
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        if (self.captureSession.isRunning == true) {
            self.captureSession.stopRunning();
        }
        
    }
    
    override var shouldAutorotate : Bool {
        
        return false
        
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        
        return .portrait
        
    }
    
    /// Shows an alert if the device doesn;t support a camera.
    func failed() {
        
        let alertController = UIAlertController().showAlert("Your device does not support scanning", message: "Please use a device with a camera.", titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        self.captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            foundCode(readableObject.stringValue);
        }
        
    }
    
    /// Completes some tasks when a barcode is found.
    ///
    /// - parameter code: The the barcode found.
    func foundCode(_ code: String) {
        
        // Checks if the product exists in the local database
        let entityProduct = "Product"
        let (existProduct, products) = DAO.sharedInstance.exist(entityProduct, data: ["barcode" : code])
        
        // If product doesn't exist in local database, saves it and then requests more details from the server
        if !existProduct {
            let entityProduct = "Product"
            let dataProduct: [String : AnyObject?] = ["name" : nil, "barcode" : code as Optional<AnyObject>, "photo" : nil]
            let product = DAO.sharedInstance.save(entityProduct, data: dataProduct)!
            
            self.findProduct(code, product: product)
            
            return
        }
        
        let product = products![0]
        // If the existing product in the local database doesn't have the name or the photo, requests it from the server
        if product.value(forKey: "name") == nil || product.value(forKey: "photo") == nil {
            self.findProduct(code, product: product)
            
            return
        }
        
        // If the last update of the product of the local database was over a week, requests it from the server
        if Date().timeIntervalSince(product.value(forKey: "lastUpdate") as! Date) > 604800 {
            self.findProduct(code, product: product)
            
            return
        }
        
        self.saveProductInList(product, barcode: code, name: nil, image: nil)
        
    }
    
    
    // MARK: Privates methods
    
    fileprivate func findProduct(_ barcode: String, product: NSManagedObject?) {
        
        ServerListManager.sharedInstance.findProduct(barcode,
            onSuccess: { (data) -> Void in
                guard let data = data as? [String : Any?] else {
                    self.saveProductInList(product, barcode: barcode, name: nil, image: nil)
                    
                    return
                }
                
                let name = data["description"] as? String
                let image_url = data["image_url"] as? String
                
                self.saveProductInList(product, barcode: barcode, name: name, image: image_url)
            },
            onFailure: { () -> Void in
                self.saveProductInList(product, barcode:   barcode, name: nil, image: nil)
            },
            onConnectionError: { (message) -> Void in
                self.saveProductInList(product, barcode: barcode, name: nil, image: nil)
            }
        )
        
    }
    
    fileprivate func saveProductInList(_ product: NSManagedObject?, barcode: String, name: String?, image: String?) {
        
        let user = AuthToken.sharedInstance.user
        
        // Updates the product if the name and image are different than nil (this means that the product was found successfully)
        if name != nil || image != nil {
            let entityProduct = "Product"
            var dataProduct: [String : AnyObject] = [String : AnyObject]()
            if let name = name {
                dataProduct.updateValue(name as AnyObject, forKey: "name")
            }
            if let image = image {
                dataProduct.updateValue(image as AnyObject, forKey: "photo")
            }
            _ = DAO.sharedInstance.update(entityProduct, propertiesToUpdate: dataProduct, argumentArray: ["barcode", barcode])
        }
        
        // If product doesn;t exists in the list, saves it. Otherwise if product exists in user's list, add 1 to quantity
        let entityNameLists = "Lists"
        var dataLists: [String : AnyObject?] = ["product" : product, "user" : user]
        let (existList, listsArray)  = DAO.sharedInstance.exist(entityNameLists, data: dataLists)
        
        if existList {
            let lists = listsArray![0]
            let count = lists.value(forKey: "count") as! Int
            let propertiesToUpdate = ["count" : count + 1]
            if let product = product as? Product, let user = user {
                let argumentArray: [Any] = ["count", count, "product", product, "user", user]
                
                _ = DAO.sharedInstance.update(entityNameLists, propertiesToUpdate: propertiesToUpdate, argumentArray: argumentArray)
            }
        } else {
            dataLists["count"] = 1 as AnyObject??
            _ = DAO.sharedInstance.save(entityNameLists, data: dataLists)
        }
        
        // Sends a notification to reload the product table
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProducts"), object: nil)
        
        /// Sends a notification to change tab selected to "Mylist tab"
        NotificationCenter.default.post(name: Notification.Name(rawValue: "tabToMyList"), object: nil)
    
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    
    // MARK: Actions
    
    @IBAction func exit(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
