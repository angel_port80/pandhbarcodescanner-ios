//
//  LogoutSegue.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 29/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import UIKit

/// Creates a custom segue for the transition between TabBar view and login view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class LogoutSegue: UIStoryboardSegue {
    
    // MARK: Methods
    
    override func perform() {
        
        self.source.present(self.destination as UIViewController, animated: false, completion: nil)
        
    }
    
}
