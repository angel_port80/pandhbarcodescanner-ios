//
//  DAO.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 03/12/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation
import UIKit
import CoreData

/// Provides access to the underlying persistence storage, which is Core Data in the case of iOS.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class DAO {
    
    // MARK: Properties
    
    /// Static instance of the class.
    static let sharedInstance = DAO()
    
    /// AppDelegate provides the Managed Object Context.
    let appDelegate: AppDelegate
    
    /// Manages a colletion of Managed Objects.
    let managedContext: NSManagedObjectContext
    
    
    // MARK: Inits
    
    /// Private initializer so nobody can instantiate the class.
    fileprivate init() {
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        
    }
    
    
    // MARK: Methods
    
    /// Saves an object from a particular entity whose name and attributes are passed through parameters.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be stored.
    ///     - data: Data of the entity to be stored. Has the key and the value of each attribute.
    /// - returns: The object stored.
    func save(_ entityName: String, data: [String : Any?]) -> NSManagedObject? {
        
        let entityDescripcion =  NSEntityDescription.entity(forEntityName: entityName, in:managedContext)!
        
        let entity = NSManagedObject(entity: entityDescripcion,
            insertInto: managedContext)
        
        for (key, value) in data {
           entity.setValue(value, forKey: key)
        }
        
        do {
            try managedContext.save()
            
            return entity
        } catch let error as NSError {
            print("DAO: An error occurred during saving. \(error)")
            return nil
        }
        
    }
    
    /// If it doesn't exist saves an object from a particular entity whose name and attributes are passed through parameters.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be stored.
    ///     - data: Data of the entity to be stored. Has the key and the value of each attribute.
    /// - returns: If it doesn't exist returns the object stored but if it exists returns the existing.
    func saveIfNotExist(_ entityName: String, data: [String : Any?]) -> NSManagedObject? {
        
        let (exist, objects) = self.exist(entityName, data: data)
        
        if exist {
            return objects![0]
        } else {
            return save(entityName, data: data)
        }
        
    }
    
    /// Fetches all objects from a particular entity.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be fetched.
    /// - returns: An array with all the fetched objects.
    func fetch(_ entityName: String) -> [NSManagedObject] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            return try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        } catch let error as NSError {
            print("DAO: An error occurred during fetching. \(error)")
            return [NSManagedObject]()
        }
        
    }
    
    /// Fetches those objects from a particular entity that fulfill a specified criterion.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be fetched.
    ///     - argumentArray: Arguments to filter by. Odd indexes are names of attributes and even indexes are values of attributes.
    /// - returns: An array with all the fetched objects.
    func fetchWithPredicate(_ entityName: String, argumentArray: [Any]) -> [NSManagedObject] {
    
        var format = "%K == %@"
        for _ in 1..<argumentArray.count/2 {
            format += " AND %K == %@"
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: format, argumentArray: argumentArray)
        
        do {
            return try self.managedContext.fetch(fetchRequest) as! [NSManagedObject]
        } catch let error as NSError {
            print("DAO: An error occurred during fetching with predicate. \(error)")
            return [NSManagedObject]()
        }
        
    }
    
    /// Updates those objects from a particular entity that fulfill a specified criterion. This function allows to update objects from a predicate created by relationships since relationships are not yet supported for NSBatchUpdateRequest. Returns the number of objects updated.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be updated.
    ///     - propertiesToUpdate: Names of the attributes to be updated and the new values.
    ///     - argumentArray: Arguments to filter by. Odd indexes are names of attributes and even indexes are values of attributes.
    /// - returns: Number of objects updated.
    func update(_ entityName: String, propertiesToUpdate: [String : Any], argumentArray: [Any]) -> Int {
        
        var format = "%K == %@"
        for _ in 1..<argumentArray.count/2 {
            format += " AND %K == %@"
        }
        
        // Fetchs objects that fulfill the specific criterion
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: format, argumentArray: argumentArray)
        
        do {
            let objectsToUpdate = try self.managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            // Changes values of fetched objects
            for objectToUpdate in objectsToUpdate {
                for (key, value) in propertiesToUpdate {
                    objectToUpdate.setValue(value, forKey: key)
                }
            }
            // Saves changes
            try managedContext.save()
            
            return objectsToUpdate.count
        } catch  let error as NSError {
            print("DAO: An error occurred during updating relationship. \(error)")
            
            return 0
        }
        
    }
    
    /// Checks if an object exists in Core Data.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be checked.
    ///     - data: Data of the entity to be checked. Has the key and the value of each attribute.
    /// - returns: If exists it returns true and the object existing, if it doesn't exist it returns false
    func exist(_ entityName: String, data: [String : Any?]) -> (Bool, [NSManagedObject]?)  {
        
        var argumentArray: [Any] = [Any]()
        for (key, value) in data {
            // Does't check attribute "count" because there could be an object in the list with the same "product" and "user" but different "count", however this would be exactly the same object.
            if key != "count" && key != "photo" && key != "name" && key != "lastUpdate" {
                if let value = value {
                    argumentArray.append(key as Any)
                    argumentArray.append(value)
                }
            }
        }
        let fetchedObjects = self.fetchWithPredicate(entityName, argumentArray: argumentArray)
        
        if fetchedObjects.count == 0 {
            return (false, nil)
        } else {
            return (true, fetchedObjects)
        }
        
    }
    
    /// Deletes those objects passed through parameters.
    ///
    /// - Parameter objectsToDelete: Objects to be deleted.
    /// - returns: True if all objects were deleted successfully.
    func delete(_ objectsToDelete: [NSManagedObject]) -> Bool {
        
        do{
            for objectToDelete in objectsToDelete {
                self.managedContext.delete(objectToDelete)
            }
            
            try self.managedContext.save()
        } catch  let error as NSError {
            print("DAO: An error occurred during deleting. \(error)")
        }
        
        return true
        
    }
    
    /// Deletes those objects from a particular entity that fulfill a specified criterion.
    ///
    /// - Parameters:
    ///     - entityName: Name of the entity to be deleted.
    ///     - argumentArray: Arguments to filter by. Odd indexes are names of attributes and even indexes are values of attributes.
    /// - returns: Number of objects deleted.
    func deleteWithPredicate(_ entityName: String, argumentArray: [Any]) -> Int {
        
        let objects = self.fetchWithPredicate(entityName, argumentArray: argumentArray)
        
        if objects.count > 0{
            
            for object in objects {
                self.managedContext.delete(object)
            }
            
            do{
                try self.managedContext.save()
            } catch  let error as NSError {
                print("DAO: An error occurred during deleting with predicate. \(error)")
                
                return 0
            }
            
            return objects.count
            
        } else {
            return 0
        }
        
    }

}
