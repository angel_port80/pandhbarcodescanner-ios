//
//  CheckboxButton.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 11/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import UIKit

/// Manages a custom button which implement a checkbox.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class CheckboxButton: UIButton {

    // MARK: Properties
    
    let checkedImage = UIImage(named: "checkboxChecked")
    let uncheckedImage = UIImage(named: "checkboxUnchecked")
    
    var isChecked: Bool = false {
        didSet {
            if self.isChecked {
                self.setImage(checkedImage, for: UIControlState())
                self.titleEdgeInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)
            } else {
                self.setImage(uncheckedImage, for: UIControlState())
                self.titleEdgeInsets = UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)
            }
        }
    }
    
    
    // MARK: Methods
    
    override func awakeFromNib() {
        
        let action = #selector(CheckboxButton.buttonClicked(_:))
        self.addTarget(self, action: action, for: UIControlEvents.touchUpInside)
        self.isChecked = false
        
    }
    
    func buttonClicked(_ sender: UIButton) {
        
        if sender == self {
            if self.isChecked {
                self.isChecked = false
            } else {
                self.isChecked = true
            }
        }
        
    }

}
