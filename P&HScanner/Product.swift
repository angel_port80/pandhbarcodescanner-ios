//
//  Product.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 30/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation
import CoreData

class Product: NSManagedObject {

    override func willSave() {
        
        let now = Date()
        
        if self.lastUpdate == nil || now.timeIntervalSince(self.lastUpdate! as Date) > 1.0 {
            self.lastUpdate = now
        }
        
    }

}
