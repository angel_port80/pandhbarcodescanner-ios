//
//  CAGradientLayerExtension.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 23/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit

extension CAGradientLayer {
    
    // MARK: Methods
    
    /// Create a gradient with the main colors of the app.
    ///
    /// - Returns: A new instance of a gradient ready to use with all the properties set.
    func standardColor() -> CAGradientLayer {
        
        // Creates the colors
        let leftColor = UIColor(red: (236/255.0), green: (28/255.0), blue: (36/255.0), alpha: 0.85)
        let rightColor = UIColor(red: (162/255.0), green: (39/255.0), blue: (42/255.0), alpha: 0.85)
        
        let gradientColors: [CGColor] = [leftColor.cgColor, rightColor.cgColor]
        
        // Creates the start point and the end point of the gradient
        let startPoint: CGPoint = CGPoint(x: 0.0, y: 0.5)
        let endPoint: CGPoint = CGPoint(x: 1.0, y: 0.5)
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint

        return gradientLayer
        
    }

}
