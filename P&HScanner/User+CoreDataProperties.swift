//
//  User+CoreDataProperties.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 20/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var email: String?
    @NSManaged var fullName: String?
    @NSManaged var username: String?
    @NSManaged var lists: NSSet?

}
