//
//  Consts.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 19/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation

/// Defines global variables used in the app.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
struct Consts {
    
    // MARK: Properties
    
    // Endpoints
    
    static let SINGLE_SIGN_ENDPOINT = "https://auth.hubtill.com/api/v1.0/pandh"
    //static let SINGLE_SIGN_ENDPOINT = "https://api.hubtill.com/dev/sso/api/v1.0/pandh"
    
    static let LISTS_ENDPOINT = "https://api.hubtill.com/lists/api"
    //static let LISTS_ENDPOINT = "https://api.hubtill.com/dev/lists/api"
    
    static let CATALOGUE_ENDPOINT =  "https://api.hubtill.com/catalogue/api"
    //static let CATALOGUE_ENDPOINT =  "https://api.hubtill.com/dev/catalogue/api"
    
    // Error messages
    
    /// Generic error message
    static let ERROR_GENERIC = "An unexpected error has occurred!"
    
    /// Error when creating the request
    static let ERROR_REQUEST_CREATE = "Unable to complete the request."
    
    /// Error whend reading the response
    static let ERROR_RESPONSE_READING = "An error occurred contacting the server."
    
    /// Error when connecting to the server
    static let ERROR_CONNECTION_SERVER = "An error occurred contacting the server. Please try again later."
    
    // Error when network is unreachable
    static let ERROR_UNREACHABLE_NETWORK = "Unable to contact network."
    
}
