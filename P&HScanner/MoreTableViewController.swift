//
//  MoreTableViewController.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 28/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import UIKit
import Foundation

/// Manages More Table View.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class MoreTableViewController: UITableViewController {

    // MARK: Properties
    
    /// Servers of the list
    var more = ["Clear list", "Log Out"]
    
    
    // MARK: Methods
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        // Hides rows without data
        let tblView =  UIView(frame: CGRect.zero)
        tableView.tableFooterView = tblView
        tableView.tableFooterView?.isHidden = true
        tableView.backgroundColor = UIColor.white

    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.more.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // List view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "MoreTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MoreTableViewCell
        
        let serverName = self.more[indexPath.row]
        
        cell.moreNameLabel.text = serverName
        cell.layoutMargins = UIEdgeInsets.zero
        cell.tag = indexPath.row
        
        return cell

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var alertController = UIAlertController()
        
        if indexPath.row == 0 {
            
            alertController = UIAlertController().showAlert(nil, message: "Are you sure you want to clear the current list?", titleCancel: "No", titleOk: "Yes", textFieldConfiguration: nil, actionCancel: nil) {
                paramAction in
                
                let entityName = "Lists"
                if let user = AuthToken.sharedInstance.user {
                    let argumentArray : [Any] = ["user", user]
                    _ = DAO.sharedInstance.deleteWithPredicate(entityName, argumentArray: argumentArray)
                }
                
                let alertController = UIAlertController().showAlert("Your list is empty", message: "Your list was cleared successfully!", titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                self.present(alertController, animated: true, completion: nil)
                
                tableView.deselectRow(at: indexPath, animated: true)
                
                // Sends a notification to reload the product table
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProducts"), object: nil)
                
            }
            
        } else if indexPath.row == 1 {
            
            // Disables user interaction when the log out is running
            self.tableView.isUserInteractionEnabled = false
            
            alertController = UIAlertController().showAlert(nil, message: "Are you sure you want to log out?", titleCancel: "No", titleOk: "Yes", textFieldConfiguration: nil,
                actionCancel: { paramAction in
                    
                    // Enables user interaction
                    self.tableView.isUserInteractionEnabled = true
                    
                },
                actionOk: { paramAction in
                    
                    var tokensToDelete: [String] = []
                    if let tokenId = AuthToken.sharedInstance.id {
                        tokensToDelete.append(tokenId)
                    }
                    // If some token was not deleted, sends it now as well
                    if let data = SecItem.sharedInstance.find("tokensNotDeleted") {
                        if let tokens = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String] {
                            tokensToDelete += tokens
                        }
                    }
                    
                    LoginManager.sharedInstance.logout(tokens: tokensToDelete,
                        onSuccess: { (data) -> Void in
                            // TODO: comment until the logout method have multiple logout
                            /*guard let data = data as? [String : Any?], let tokensExpireSuccessful = data["successful"] as? [String] else {
                                DispatchQueue.main.async(execute: {
                                    let alertController = UIAlertController().showAlert("", message: Consts.ERROR_GENERIC, titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                                    self.present(alertController, animated: true, completion: nil)
                                })
                                
                                return
                            }
                            
                            // Gets token which wasn't expired successfully
                            var tokensExpireFailed: [String] = []
                            for tokenToDelete in tokensToDelete {
                                if !tokensExpireSuccessful.contains(tokenToDelete) {
                                    tokensExpireFailed.append(tokenToDelete)
                                }
                            }
                            
                            self.manageTokenNotDeleted(tokensExpireFailed)
                            */
                            self.localLogOut()
                        },
                        onFailure: {
                            self.manageTokenNotDeleted(tokensToDelete)
                            
                            self.localLogOut()
                        },
                        onConnectionError: { (message) -> Void in
                            self.manageTokenNotDeleted(tokensToDelete)
                            
                            self.localLogOut()
                        }
                    )
                
                }
            )
            
        }
        
        self.present(alertController, animated: true, completion: nil)
        self.tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    // MARK: Privates Methods
    
    // Deletes information about the local session
    fileprivate func localLogOut() {
        
        AuthToken.sharedInstance.id = nil
        AuthToken.sharedInstance.user = nil
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        DispatchQueue.main.async(execute: {
            if let _ = appDelegate.window?.rootViewController as? LoginViewController {
                if let tabBarController = self.tabBarController {
                    tabBarController.dismiss(animated: false, completion: nil)
                }
            } else {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                
                appDelegate.window?.rootViewController = loginViewController
            }
        })
        
        // Enables user interaction when log out is finished
        self.tableView.isUserInteractionEnabled = true
        
    }
    
    // Storages the tokens which wasn't deleted from the server properly
    fileprivate func manageTokenNotDeleted(_ tokens: [String]) {
        
        if tokens.isEmpty {
            _ = SecItem.sharedInstance.deleteSafely("tokensNotDeleted")
        } else {
            SecItem.sharedInstance.add("tokensNotDeleted", value: tokens)
        }
        
    }

}
