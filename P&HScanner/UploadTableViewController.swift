//
//  UploadTableViewController.swift
//  PandHBarcodeScanner
//
//  Created by Ángel David Macho Esperilla on 13/04/2017.
//  Copyright © 2017 Port80. All rights reserved.
//

import UIKit

/// Manages Upload Table View.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class UploadTableViewController: UITableViewController {

    // MARK: Properties
    
    /// Organisations of the user
    var organisations = AuthToken.sharedInstance.organisations
    
    
    // MARK: Methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Hides rows without data
        let tblView =  UIView(frame: CGRect.zero)
        tableView.tableFooterView = tblView
        tableView.tableFooterView?.isHidden = true
        tableView.backgroundColor = UIColor.white

    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.organisations.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "UploadTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        // Fetches the appropriate product for the data source layout.
        let organisation = self.organisations[indexPath.row]
        
        cell.textLabel?.text = organisation["name"] as? String
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let itemsOpt = ProductTableViewController().getBarcodesQty()
        guard let items = itemsOpt else {
            DispatchQueue.main.async(execute: {
                let alertController = UIAlertController().showAlert("Unable to upload list", message: "Your current list is empty!", titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                self.present(alertController, animated: true, completion: nil)
                self.tableView.deselectRow(at: indexPath, animated: true)
            })
            return
        }
        
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let organisation = organisations[indexPath.row]
        ServerListManager.sharedInstance.uploadList(deviceId, items: items, organisation: organisation,
            onSuccess: { (data) -> Void in
                guard let data = data as? [String : String], let redirectHash = data["redirect_hash"] else {
                    self.unexpectedError()
                    
                    return
                }
                
                let alertController = UIAlertController().showAlert("Upload successful!", message: "Do you want to clear your current list?", titleCancel: "No", titleOk: "Yes", textFieldConfiguration: nil,
                    actionCancel: { paramAction in
                        UIApplication.shared.openURL(NSURL(string: "https://order.palmerharvey.co.uk/scannerapp?redirect=\(redirectHash)")! as URL)
                    },
                    actionOk: { paramAction in
                        
                        let entityName = "Lists"
                        if let user = AuthToken.sharedInstance.user {
                            let argumentArray : [Any] = ["user", user]
                            _ = DAO.sharedInstance.deleteWithPredicate(entityName, argumentArray: argumentArray)
                        }
                        
                        // Sends a notification to reload the product table
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProducts"), object: nil)
                        
                        
                        UIApplication.shared.openURL(NSURL(string: "https://order.palmerharvey.co.uk/scannerapp?redirect=\(redirectHash)")! as URL)
                    }
                )
                
                self.present(alertController, animated: true, completion: nil)
            },
            onFailure: { () -> Void in
                self.unexpectedError()
            },
            onConnectionError: { (message) -> Void in
                // It is unsafe perform any UI operation on a background thread, which NSURLSession callbacks default to. So it will move the code that interacts with the UI to the main thread
                DispatchQueue.main.async(execute: {
                    let alertController = UIAlertController().showAlert("Connection error", message: message, titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                    self.present(alertController, animated: true, completion: nil)
                })
            }
        )
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    // MARK: Private Methods
    
    fileprivate func unexpectedError() {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController().showAlert("", message: Consts.ERROR_GENERIC, titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
            self.present(alertController, animated: true, completion: nil)
        })
    }
   
}
