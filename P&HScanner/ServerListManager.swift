//
//  ServerListManager.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 17/12/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation

/// Manages operations with lists and products in different platforms.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
open class ServerListManager {

    // MARK: Properties
    
    /// Static instance of the class.
    static let sharedInstance = ServerListManager()
    
    
    // MARK: Inits
    
    /// Private initializer so nobody can instantiate the class.
    fileprivate init() {
        
    }
    
    
    // MARK: Methods

    ///  Adds to P&H platform a new list.
    ///
    /// - Parameters:
    ///     - deviceId: A unique identifier for the mobile/device.
    ///     - items: List of the barcodes and qty which will be added to the list.
    ///     - organisation: Organisation where upload the list.
    ///     - onSuccess: Code to execute in case it creates a new list successfully.
    ///     - onFailure: Code to execute in case it doesn't create a new list successfully.
    ///     - onConnectionError: Code to execute in case of any connection error with the server.
    func uploadList(_ deviceId: String, items: [[String : Any]], organisation: [String : Any],
        onSuccess: @escaping (_ data: Any?) -> Void,
        onFailure: @escaping () -> Void,
        onConnectionError: @escaping (_ message: String) -> Void) {
            
            // Creates HTTP request
            let organisationId = organisation["id"] as! Int
            let associatedInfo = organisation["associated_info"] as! [String : Any]
            let sourceId = associatedInfo["source_id"] as! Int
            let rangeId = associatedInfo["range_id"] as! String
            let sourceLocationId = associatedInfo["source_location_id"] as! String
            let sourceCustomerId = associatedInfo["source_customer_id"] as! String
            let sourceAccountNumber = associatedInfo["source_account_number"] as! String
            let requiredPoRef = associatedInfo["requires_po_ref"] as! Bool
        
            let context: [String : Any] = ["auth_token" : AuthToken.sharedInstance.id!, "organisation_id" : organisationId, "source_id" : sourceId, "range_id" : rangeId, "source_location_id" : sourceLocationId, "source_customer_id" : sourceCustomerId, "source_account_number" : sourceAccountNumber, "requires_po_ref" : requiredPoRef]
        
            let params: [String : Any] = ["method" : "upload_scan_items", "device_id" : deviceId, "items" : items, "context" : context]
            let uploadListRequest = RequestHTTP(url: Consts.LISTS_ENDPOINT, operation: .post, params: params)
            
            if let uploadListRequest = uploadListRequest {
                uploadListRequest.taskWithRequest(onSuccess: onSuccess, onFailure: onFailure, onConnectionError: onConnectionError)
            } else {
                onConnectionError(Consts.ERROR_REQUEST_CREATE)
            }
            
    }
    
    /// Gets properties of a product from the database of the server given its barcode.
    ///
    /// - Parameters:
    ///     - barcode: Barcode of the product.
    func findProduct(_ barcode: String,
        onSuccess: @escaping (_ data: Any?) -> Void,
        onFailure: @escaping() -> Void,
        onConnectionError: @escaping (_ message: String) -> Void) {
        
            // Creates HTTP request
            let params: [String : Any] = ["method" : "get_product_by_barcode", "barcode" : barcode]
            let findProductRequest = RequestHTTP(url: Consts.CATALOGUE_ENDPOINT, operation: .post, params: params)
        
            if let findProductRequest = findProductRequest {
                findProductRequest.taskWithRequest(onSuccess: onSuccess, onFailure: onFailure, onConnectionError: onConnectionError)
            } else {
                onConnectionError(Consts.ERROR_REQUEST_CREATE)
            }

    }

}
