//
//  Token.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 19/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation

/// Defines the Authorization Token.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class AuthToken {
    
    // MARK: Properties
    
    /// Static instance of the class.
    static let sharedInstance = AuthToken()
    
    /// Id of the Authorization Token. It is a random sequence of characters.
    var id: String? {
        get {
            if let data = SecItem.sharedInstance.find("authTokenId") {
                return String(data: data, encoding: String.Encoding.utf8)
            } else {
                return nil
            }
        }
        set {
            if let newValue = newValue {
                SecItem.sharedInstance.add("authTokenId", value: newValue)
            } else {
                _ = SecItem.sharedInstance.deleteSafely("authTokenId")
            }
        }
    }
    
    /// Authorized user.
    var user: User? {
        get {
            let defaults = UserDefaults.standard
            let username = defaults.string(forKey: "username")
            
            if let username = username {
                let entityNameUser = "User"
                let argumentArray: [AnyObject] = ["username" as AnyObject, username as AnyObject]
                let userArray = DAO.sharedInstance.fetchWithPredicate(entityNameUser, argumentArray: argumentArray)
                let user = userArray[0] as? User
                return user
            } else {
                return nil
            }
        }
        set {
            let defaults = UserDefaults.standard
            
            if let newValue = newValue {
                let username = newValue.value(forKey: "username")
                defaults.set(username, forKey: "username")
                
                // Checks and sets if it is the first time the app is running in this device
                if !defaults.bool(forKey: "notFirstRun") {
                    defaults.set(true, forKey: "notFirstRun")
                }
            } else {
                defaults.removeObject(forKey: "username")
            }
        }
    }
    
    /// Organisations of the user.
    var organisations: [[String : Any]] {
        get {
            let defaults = UserDefaults.standard
            let organisations = defaults.array(forKey: "organisations") as! [[String : Any]]
            return organisations
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "organisations")
        }
    }
    
    
    // MARK: Inits
    
    /// Private initializer so nobody can instantiate the class.
    fileprivate init() {
    
    }
    
    
    // MARK: Methods
    
    /// Checks if there is a user authenticated
    ///
    /// - returns: True if there is a user authenticated or false if there isn't.
    func isAuthenticated() -> Bool {
        
        if self.id == nil {
            return false
        }
        
        return true
        
    }
    
}
