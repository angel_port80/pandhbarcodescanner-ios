//
//  ServerTableViewCell.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 26/12/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit

/// Manages rows in Upload table view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class ServerTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var serverNameLabel: UILabel!
    
    // MARK: Methods
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)

    }

}
