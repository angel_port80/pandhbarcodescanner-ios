//
//  LoginViewController.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 04/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit

/// Manages Login view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class LoginViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    
    // MARK: Properties
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var textInputTableView: UITableView!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOrganizationTableView: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintFromCheckboxToOrganizationTextField: NSLayoutConstraint!
    
    // Default value of the bottom's constraint
    var bottomConstraintConstantDefault: CGFloat = 0.0
    
    // Placeholders of different text fields
    var itemsLogin: [String] = ["Username", "Password"]
    
    /// Gradient of the background
    let gradientLayer: CAGradientLayer = CAGradientLayer().standardColor()
    
    
    // MARK: Methods
    
    override func viewDidLoad() {
         
        super.viewDidLoad()
        
        // Sets the background gradient color
        //gradientLayer.frame = self.view.bounds
        //self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        // Rounds corners of the TableView
        self.textInputTableView.layer.cornerRadius = 2
        
        // Rounds corners of the button
        self.loginButton.layer.cornerRadius = 2
        
        // Sets the background gradient color to the button
        self.gradientLayer.frame = self.loginButton.bounds
        self.gradientLayer.cornerRadius = 2
        self.loginButton.layer.insertSublayer(gradientLayer, at: 0)
    
        self.bottomConstraintConstantDefault = self.bottomConstraint.constant
        
        // Creates notification when keyboard appears and disappears
        let selectorKeyboardWillShow = #selector(LoginViewController.keyboardWillShow(_:))
        NotificationCenter.default.addObserver(self, selector: selectorKeyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        let selectorKeyboardWillHide = #selector(LoginViewController.keyboardWillHide(_:))
        NotificationCenter.default.addObserver(self, selector: selectorKeyboardWillHide, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    deinit {
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Gets the UITextFields of the screen
        let usernameUITextField = self.textInputTableView.viewWithTag(200) as? UITextField
        let passwordUITextField = self.textInputTableView.viewWithTag(201) as? UITextField
        
        // Reloads the whole view when it comes from a dismiss (from logout screen)
        if usernameUITextField?.text != "" {
           usernameUITextField?.text = nil
        }
        if passwordUITextField?.text != "" {
           passwordUITextField?.text = nil
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        
        // Adjusts the bounds of the gradient when the screen rotates
        gradientLayer.frame = self.loginButton.bounds
        self.gradientLayer.cornerRadius = 2
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // Closes keyboard when touched on empty area
        self.view.endEditing(true)
        
    }
    
    override var shouldAutorotate : Bool {
        
        if (self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && self.view.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular) {
            return true
        } else {
            return false
        }
        
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        self.adjustingHeight(true, notification: notification)
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        
        self.adjustingHeight(false, notification: notification)
        
    }
    
    
    // MARK: Actions
    
    /// Launches an action when the login button is tapped.
    ///
    /// - parameter sender: The pressed button.
    @IBAction func logIn(_ sender: UIButton) {
        let tittleLoginButton = loginButton.title(for: UIControlState())
        loginButton.setTitle(nil, for: UIControlState())
        self.activityIndicator.startAnimating()
        
        // Gets the UITextFields of the screen
        let usernameUITextField = self.textInputTableView.viewWithTag(200) as! UITextField
        let passwordUITextField = self.textInputTableView.viewWithTag(201) as! UITextField
        
        // Gets the text from the UITextFields of the screen
        let usernameTextField = usernameUITextField.text ?? ""
        let passwordTextField = passwordUITextField.text ?? ""
        
        LoginManager.sharedInstance.login(username: usernameTextField, password: passwordTextField,
            onSuccess: { (data) -> Void in
                // TODO: organisations getting temporaly
                guard let data = data as? [String : Any?], let session = data["session"] as? [String : Any?], let organisations = data["organisations"] as? [[String : Any]] else {
                    self.unexpectedError(tittleLoginButton)
                    
                    return
                }
                
                let dataUser = session["user"] as! [String : Any]
                let username = dataUser["username"] as? String
                let email = dataUser["email"] as? String
                let fullName = dataUser["fullname"] as? String
                
                // Saves the user in Core Data if it is the first time it has connected
                let entityName = "User"
                let user: [String : Any?] = ["email" : email, "fullName" : fullName, "username" : username, "lists" : nil]
                
                // Saves the user for futures HTTP requests
                AuthToken.sharedInstance.user = DAO.sharedInstance.saveIfNotExist(entityName, data: user) as? User
                
                // Gets the token from the server data and saves it for futures HTTP requests
                AuthToken.sharedInstance.id = session["auth_token"] as? String
                
                // TODO: organisations keep temporaly
                AuthToken.sharedInstance.organisations = organisations
                
                DispatchQueue.main.async(execute: {
                    UIView.setAnimationsEnabled(false)
                    self.performSegue(withIdentifier: "loginSegue", sender: self)
                    UIView.setAnimationsEnabled(true)
                    
                    self.activityIndicator.stopAnimating()
                    self.loginButton.setTitle(tittleLoginButton, for: UIControlState())
                })
            },
            onFailure: {
                // It is unsafe perform any UI operation on a background thread, which NSURLSession callbacks default to. So it will move the code that interacts with the UI to the main thread
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stopAnimating()
                    self.loginButton.setTitle(tittleLoginButton, for: UIControlState())
                    
                    let alertController = UIAlertController().showAlert("Please try again", message: "Please check your username and password and try again", titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                    self.present(alertController, animated: true, completion: nil)
                })
            },
            onConnectionError: { (message) -> Void in
                // It is unsafe perform any UI operation on a background thread, which NSURLSession callbacks default to. So it will move the code that interacts with the UI to the main thread
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stopAnimating()
                    self.loginButton.setTitle(tittleLoginButton, for: UIControlState())
                    
                    let alertController = UIAlertController().showAlert("Connection error", message: message, titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                    self.present(alertController, animated: true, completion: nil)
                })
            }
        )
        
    }
    
    @IBAction func forgotPassword(_ sender: AnyObject) {
        
        if let url = URL(string: "https://order.palmerharvey.co.uk/forgotpassword") {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBAction func signUp(_ sender: AnyObject) {
        
        if let url = URL(string: "") {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let passwordTextField = self.textInputTableView.viewWithTag(201) as! UITextField
        passwordTextField.enablesReturnKeyAutomatically = true
        passwordTextField.returnKeyType = .go
       
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        let usernameTextField = self.textInputTableView.viewWithTag(200) as! UITextField
        let passwordTextField = self.textInputTableView.viewWithTag(201) as! UITextField
        
        if let username = usernameTextField.text, let password = passwordTextField.text {
            if username.isEmpty || password.isEmpty {
                self.loginButton.isEnabled = false
            } else {
                self.loginButton.isEnabled = true
            }
        } else {
            self.loginButton.isEnabled = false
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let usernameTextField = self.textInputTableView.viewWithTag(200) as! UITextField
        let passwordTextField = self.textInputTableView.viewWithTag(201) as! UITextField
        
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        }
        
        if textField == passwordTextField {
            if let username = usernameTextField.text {
                if !username.isEmpty {
                    self.logIn(self.loginButton)
                }
            }
        }
        
        return false
        
    }
    
    
    // MARK: UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (tableView == self.textInputTableView) {
            return 2
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let action = #selector(LoginViewController.textFieldDidChange(_:))
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as! TextInputTableViewCell
            
        cell.textField.placeholder = itemsLogin[indexPath.row]
        // Creates a notification each time a textfield is modified
        cell.textField.addTarget(self, action: action, for: UIControlEvents.editingChanged)
        cell.textField.tag = 200 + indexPath.row
        cell.layoutMargins = UIEdgeInsets.zero
        // Sets secure text entry to the password text field
        if indexPath.row == 1 {
            cell.textField.isSecureTextEntry = true
        }
            
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.compact || self.view.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.compact {
            return 40
        } else {
            return 50
        }
        
    }
    
    
    // MARK: Private Methods
    
    fileprivate func adjustingHeight(_ show: Bool, notification: Notification) {
        
        // Gets notification information in an dictionary
        var userInfo = notification.userInfo!
        // From information dictionary gets keyboard’s size
        let keyboardFrame: CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        // Gets the time required for keyboard pop up animation
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        // Animation moving constraint at same speed of moving keyboard & change bottom constraint accordingly.
        if !(show && self.bottomConstraint.constant != self.bottomConstraintConstantDefault) {
            if show {
                self.bottomConstraint.constant = keyboardFrame.size.height + self.bottomConstraintConstantDefault / 2
            } else {
                self.bottomConstraint.constant = self.bottomConstraintConstantDefault
            }
            UIView.animate(withDuration: animationDurarion, animations: {
                self.view.layoutIfNeeded()
            }) 
        }
        
        self.hideLogoIfSmall()
        
    }
    
    fileprivate func hideLogoIfSmall() {
        
        let logoHeight = self.logo.frame.height
        if logoHeight < 50 {
            self.logo.isHidden = true
        } else {
            self.logo.isHidden = false
        }
        
    }
    
    fileprivate func unexpectedError(_ tittleLoginButton: String?) {
        DispatchQueue.main.async(execute: {
            self.activityIndicator.stopAnimating()
            self.loginButton.setTitle(tittleLoginButton, for: UIControlState())
            
            let alertController = UIAlertController().showAlert("", message: Consts.ERROR_GENERIC, titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
}
