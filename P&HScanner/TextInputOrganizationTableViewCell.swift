//
//  TextInputOrganizationTableViewCell.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 11/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import UIKit

/// Manages row in Text Input Organization table view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class TextInputOrganizationTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var textField: UITextField!
    
    
    // MARK: Methods
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
    }

}
