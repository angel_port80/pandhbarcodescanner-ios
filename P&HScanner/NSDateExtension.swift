//
//  NSDateExtension.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 27/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import Foundation

extension Date {
    
    // MARK: Methods
    
    /// Returns if the receiver date is greater than another given date.
    ///
    /// - Returns: True if the receiver date is greater than the given date.
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool {
        
        return self.compare(dateToCompare) == ComparisonResult.orderedDescending
        
    }
    
    /// Returns if the receiver date is smaller than another given date.
    ///
    /// - Returns: True if the receiver date is smaller than the given date.
    func isLessThanDate(_ dateToCompare : Date) -> Bool {
        
        return self.compare(dateToCompare) == ComparisonResult.orderedAscending
    }
    
    /// Adds the days given to the receiver date.
    ///
    /// - Returns: The new date.
    func addDays(_ daysToAdd : Int) -> Date {
        
        let secondsInDays : TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        
        return self.addingTimeInterval(secondsInDays)
        
    }
    
}
