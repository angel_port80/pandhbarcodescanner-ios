//
//  ListTableViewCell.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 21/12/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit

/// Manages rows in Server List table view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class ListTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet var imagesView: [UIImageView]!
    
    
    // MARK: Methods
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
    }

}
