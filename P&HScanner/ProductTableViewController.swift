//
//  ProductTableViewController.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 25/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit
import CoreData

/// Manages Product Table view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class ProductTableViewController: UITableViewController {

    // MARK: Properties
    
    @IBOutlet weak var barcodeTextField: UITextField!
    @IBOutlet weak var cancelConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var cancelTextFieldConstrainHorizontalSpace: NSLayoutConstraint!
    
    /// Products of the list
    var products = [NSManagedObject]()
    
    /// Gradient of the background
    let gradientLayer: CAGradientLayer = CAGradientLayer().standardColor()
    
    
    // MARK: Methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.loadProducts()
        
        // Reload content of the table when comes from the scanner and update
        let selectorReloadProducts = #selector(ProductTableViewController.reloadProducts(_:))
        NotificationCenter.default.addObserver(self, selector: selectorReloadProducts, name: NSNotification.Name(rawValue: "reloadProducts"), object: nil)
        let selectorKeyboardWillShow = #selector(ProductTableViewController.keyboardWillShow(_:))
        NotificationCenter.default.addObserver(self, selector: selectorKeyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        let selectorKeyboardWillHide = #selector(ProductTableViewController.keyboardWillHide(_:))
        NotificationCenter.default.addObserver(self, selector: selectorKeyboardWillHide, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let tbc = tabBarController as! TabBarController
        tbc.listIsEmpty = products.isEmpty
        
    }

    /// Reloads table of products
    func reloadProducts(_ notification: Notification) {
        
        self.loadProducts()
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
    }
    
    
    // MARK: Actions
    
    @IBAction func cancelButton(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.products.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "ProductTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ProductTableViewCell
        
        // Fetches the appropriate product for the data source layout.
        let productList = self.products[indexPath.row]
        
        let product = productList.value(forKey: "product") as? NSManagedObject
        
        cell.productNameLabel.text = product?.value(forKey: "name") as? String ?? "Unknown product"
        cell.barcodeLabel.text = product?.value(forKey: "barcode") as? String
        cell.quantityLabel.text = String(productList.value(forKey: "count") as! Int)
        cell.stepper.value = productList.value(forKey: "count") as! Double
        
        if let url = product?.value(forKey: "photo") as? String {
            cell.photoImageView.sd_setImage(with: URL(string: url))
        } else if product?.value(forKey: "name") == nil {
            cell.photoImageView.image = UIImage(named: "warning")
        } else {
            cell.photoImageView.image = nil
        }
        
        return cell
        
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        // Return false if you do not want the specified item to be editable.
        return true
        
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            // Delete the row from the Core Data
            let productFromList = [self.products[indexPath.row]]
            let success = DAO.sharedInstance.delete(productFromList)
            
            if success {
                self.products.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } else {
                let alertController = UIAlertController().showAlert(Consts.ERROR_GENERIC, message: "This product could not be removed", titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
                self.present(alertController, animated: true, completion: nil)

            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
     
        let deleteButton = UITableViewRowAction(style: .default, title: "Delete",
            handler: { (action, indexPath) in
                self.tableView.dataSource?.tableView?(
                    self.tableView,
                    commit: .delete,
                    forRowAt: indexPath
                )
        })
        deleteButton.backgroundColor = UIColor(red: 206/255, green: 31/255, blue: 36/255, alpha: 1)
        
        return [deleteButton]
        
    }
    
    /// Gets barcodes and quantities of all products of the list.
    ///
    /// - returns: An array with all the barcodes and quantities of each product of the list.
    func getBarcodesQty() -> [[String : Any]]? {
    
        var result: [[String : Any]]? = []
        
        self.loadProducts()
        
        if self.products.isEmpty {
            return nil
        }
        
        for i in 0 ..< self.products.count {
            let productList = self.products[i]
            let product = productList.value(forKey: "product") as? NSManagedObject
            
            if let product = product {
                if let barcode = product.value(forKey: "barcode"), let qty = productList.value(forKey: "count") {
                    let dictionary: [String : Any] = ["barcode" : barcode as Any, "qty" : qty as Any]
                    result?.append(dictionary)
                }
            }
        }
        
        return result
        
    }
    
    /// Runs when the keyboard is going to appear
    func keyboardWillShow(_ notification: Notification) {
        
        if (self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.compact) {
            // Size class compact (iphones)
            self.cancelConstraintWidth.constant = 47
        } else {
            // Size class regular (ipads)
            self.cancelConstraintWidth.constant = 60
        }
        self.cancelTextFieldConstrainHorizontalSpace.constant = 5
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) 
        
    }
    
    /// Runs when the keyboard is going to disappear
    func keyboardWillHide(_ notification: Notification) {
        
        self.cancelConstraintWidth.constant = 0
        self.cancelTextFieldConstrainHorizontalSpace.constant = 0
        self.barcodeTextField.text = nil
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) 
        
    }
    
    
    // MARK: UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let barcodeStr = self.barcodeTextField.text!
        let barcodeCount = barcodeStr.characters.count
        if let _ = Int(barcodeStr), barcodeCount == 8 || (barcodeCount >= 12 && barcodeCount <= 14) {
            ScannerViewController().foundCode(barcodeStr)
            
            self.barcodeTextField.text = nil
            self.view.endEditing(true)
        } else {
            let alertController = UIAlertController().showAlert(nil, message: "Invalid barcode", titleCancel: nil, titleOk: "OK", textFieldConfiguration: nil, actionCancel: nil, actionOk: nil)
            self.present(alertController, animated: true, completion: nil)
        }
        
        return false
        
    }

    
    // MARK: Private Methods
    
    fileprivate func loadProducts() {
        
        let entittyName = "Lists"
        let userAuth: User! = AuthToken.sharedInstance.user
        let argumentArray: [AnyObject] = ["user" as AnyObject, userAuth]
        products = DAO.sharedInstance.fetchWithPredicate(entittyName, argumentArray: argumentArray)
        
    }

}
