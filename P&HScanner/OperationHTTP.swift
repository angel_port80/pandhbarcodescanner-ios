//
//  File.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 18/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation

/// Defines each possible operation of an HTTP Request.
///
/// - GET: For GET HTTP request.
///
/// - POST: For POST HTTP request.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
enum OperationHTTP {
    
    case get, post
    
}
