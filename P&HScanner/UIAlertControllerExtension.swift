//
//  UIAlertControllerExtension.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 19/02/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import Foundation

extension UIAlertController {
    
    // MARK: Methods
    
    /// Shows an alert which can be customized.
    ///
    /// - Parameters:
    ///     - titleMessage: Title of the message which will appear in the header of the alert.
    ///     - message: Content of the message which will appear in the body of the alert.
    ///     - titleCancel: Title of the cancel button.
    ///     - titleOk: Title of the ok button.
    ///     - textFieldConfiguration: Code to set a text field
    ///     - actionCancel: Action to launch once the cancel button is clicked.
    ///     - actionOk: Action to launch once the ok button is clicked.
    func showAlert(_ titleMessage: String?, message: String, titleCancel: String?, titleOk: String?, textFieldConfiguration: ((UITextField) -> Void)?,actionCancel: ((UIAlertAction) -> Void)?, actionOk: ((UIAlertAction) -> Void)?) -> UIAlertController {
        
        let alertController: UIAlertController = UIAlertController(title: titleMessage, message: message, preferredStyle: .alert)
        alertController.view.tintColor = UIColor(red: (206/255.0), green: (31/255.0), blue: (36/255.0), alpha: 0.85)
        if titleCancel != nil {
            let cancelAction = UIAlertAction(title: titleCancel, style: UIAlertActionStyle.default, handler: actionCancel)
            alertController.addAction(cancelAction)
        }
        if titleOk != nil {
            let okAction = UIAlertAction(title: titleOk, style: UIAlertActionStyle.default, handler: actionOk)
            alertController.addAction(okAction)
        }
        if textFieldConfiguration != nil {
            alertController.addTextField(configurationHandler: textFieldConfiguration)
        }
        
        return alertController
        
    }
    
}
