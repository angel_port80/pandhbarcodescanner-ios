//
//  SecItem.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 11/12/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation

/// Provides functions to manage sensitive data in the keychain, a safe way to store information.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class SecItem {
    
    // MARK: Properties
    
    /// Static instance of the class.
    static let sharedInstance = SecItem()
    
    
    // MARK: Inits
    
    /// Private initializer so nobody can instantiate the class.
    fileprivate init() {
        
    }
    
    
    // MARK: Methods
    
    /// Stores securely data in the keychain.
    ///
    /// - Parameters:
    ///     - key: Specifies the key to the value to be stored.
    ///     - value: Value to be stored.
    func add(_ key: String, value: Any) {
        
        let service = Bundle.main.bundleIdentifier!
        
        var valueData: Data! = Data()
        if let value = value as? String {
            valueData = value.data(using: String.Encoding.utf8, allowLossyConversion: false)
        } else  if let value = value as? [String] {
            valueData = NSKeyedArchiver.archivedData(withRootObject: value)
        }
        
        let secItem = [
            kSecClass as String : kSecClassGenericPassword as String,
            kSecAttrService as String : service,
            kSecAttrAccount as String : key,
            kSecValueData as String : valueData
        ] as [String : Any]
        let result: UnsafeMutablePointer<AnyObject?>? = nil
        let status = Int(SecItemAdd(secItem as CFDictionary, result))
        
        if status == Int(errSecDuplicateItem){
            self.update(key, newData: value)
        } else {
            if status != Int(errSecSuccess) {
                print("SectItem: An error occurred during saving with code \(status).")
            }
        }
        
    }
    
    /// Given a key, updates its value.
    ///
    /// - Parameters:
    ///     - key: Specifies the key to the value to be updated.
    ///     - value: Value to be updated.
    func update(_ key: String, newData: Any) {
        
        let service = Bundle.main.bundleIdentifier!
        let query = [
            kSecClass as String : kSecClassGenericPassword as String,
            kSecAttrService as String : service,
            kSecAttrAccount as String : key,
            //kSecReturnData as String : kCFBooleanTrue,
        ]
        
        var valueData: Data! = Data()
        if let newData = newData as? String {
            valueData = newData.data(using: String.Encoding.utf8,
                allowLossyConversion: false)
        } else  if let newData = newData as? [String] {
            valueData = NSKeyedArchiver.archivedData(withRootObject: newData)
        }
        
        let update = [
            kSecValueData as String : valueData
        ]
        
        let updated = Int(SecItemUpdate(query as CFDictionary, update as CFDictionary))
        
        if updated != Int(errSecSuccess) {
            print("SectItem: An error occurred during updating with code \(updated).")
        }
        
    }
    
    /// Deletes data in the keychain.
    ///
    /// - Parameters:
    ///     - key: Specifies the key to be deleted.
    func delete(_ key: String) {
        
        let service = Bundle.main.bundleIdentifier!
        
        let query = [
            kSecClass as String : kSecClassGenericPassword as String,
            kSecAttrService as String : service,
            kSecAttrAccount as String : key,
        ]
        
        let deleted = Int(SecItemDelete(query as CFDictionary))
        
        if deleted != Int(errSecSuccess) {
            print("SectItem: An error occurred during deleting with code \(deleted).")
        }
        
    }
    
    /// Queries the keychain to find an existing item.
    ///
    /// - Parameters:
    ///     - keyToSearchFor: The key to search for.
    /// - returns: If the key exists returns its value, if it doesn't exist returns nil.
    func find(_ keyToSearchFor: String) -> Data? {
        
        let service = Bundle.main.bundleIdentifier!
        let query = [
            kSecClass as String : kSecClassGenericPassword as String,
            kSecAttrService as String : service,
            kSecAttrAccount as String : keyToSearchFor,
            kSecReturnData as String : kCFBooleanTrue,
        ] as [String : Any]
        
        var returnedData: AnyObject?
        let results = Int(SecItemCopyMatching(query as CFDictionary, &returnedData))
        
        if results == Int(errSecSuccess){
            return returnedData as? Data
        } else {
            if results != Int(errSecItemNotFound) {
                print("SectItem: An error occurred during finding with code \(results).")
            }
            
            return nil
        }
    }
    
    /// Given a key, updates its value. Before update, checks if the key exists.
    ///
    /// - Parameters:
    ///     - key: Specifies the key to the value to be updated.
    ///     - value: Value to be updated.
    /// - returns: True if there was something to update.
    func updateSafely(_ key: String, newData: AnyObject) -> Bool {
        
        if self.find(key) != nil {
            self.update(key, newData: newData)
            
            return true
        }
        
        return false
        
    }
    
    /// Given a key, deletes it. Before delete, checks if the key exists.
    ///
    /// - Parameters:
    ///     - key: Specifies the key to be deleted.
    /// - returns: True if there was something to delete.
    func deleteSafely(_ key: String) -> Bool {
        
        if self.find(key) != nil {
            self.delete(key)
            
            return true
        }
        
        return false
        
    }

}
