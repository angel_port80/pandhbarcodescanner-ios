//
//  ProductTableViewCell.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 25/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import UIKit

/// Manages rows in My List table view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class ProductTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var barcodeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    
    // MARK: Methods
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
    }
    
    
    // MARK: Actions

    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        
        let user = AuthToken.sharedInstance.user
        
        // Updates the quantity of the product in Database
        let entityName = "Lists"
        let propertiesToUpdate = ["count" : Int(sender.value)]
        if let user = user {
            let argumentArray = ["product.barcode", self.barcodeLabel.text!, "user", user] as [Any]
            _ = DAO.sharedInstance.update(entityName, propertiesToUpdate: propertiesToUpdate, argumentArray: argumentArray)
        }
        
        self.quantityLabel.text = String(Int(sender.value))
        
    }
    
}
