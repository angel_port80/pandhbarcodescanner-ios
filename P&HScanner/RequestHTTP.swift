//
//  RequestHTTP.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 18/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation
import SystemConfiguration

/// Wrapper class around NSURLSession to better and faster handle of HTTP requests. You can use RequestHTTP object to create an POST or GET HTTP Request.
///
/// - seealso: [NSURLSession](https://developer.apple.com/library/ios/documentation/Foundation/Reference/NSURLSession_class/)
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class RequestHTTP {
    
    // MARK: Properties
    
    /// Instance of NSURLSession class which provide support for HTTP requests.
    fileprivate let session = URLSession.shared
    
    /// Represents a URL load request.
    fileprivate let request: NSMutableURLRequest
    
    /// Represents a URL that can potentially contain the location of a resource on a remote server.
    fileprivate let url: URL!
    
    
    // MARK: Inits
    
    /// Initializes an RequestHTTP object. Sets header, body and operation of the Request. Return nil if some exception occurs during JSON object creation.
    ///
    /// - Parameters:
    ///     - url: Url where the request is sent.
    ///     - operation: Operation of the request.
    ///     - params: Data you need to send to the server.
    init?(url: String, operation: OperationHTTP, params: [String : Any]) {
        
        self.url = URL(string: url)
        self.request = NSMutableURLRequest(url: self.url)
        
        switch operation {
        case .get:
            self.request.httpMethod = "GET"
        case .post:
            self.request.httpMethod = "POST"
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        // Create and add context to the request
        // TODO: descomentar esto cuando el tema de organizacion y context este solucionado
        var params = params
        if let token = AuthToken.sharedInstance.id {
            /*let context: [String : String] = ["auth_token" : token]
            params["context"] = [context]*/
        
            
            // TODO: this is temporal, until the methods does not need organisaton info in the context
            if params["context"] == nil {
                let organisations = AuthToken.sharedInstance.organisations
            
                let organisation = organisations[0]
                let organisationId = organisation["id"] as! Int
                let associatedInfo = organisation["associated_info"] as! [String : Any]
                let sourceId = associatedInfo["source_id"] as! Int
                let rangeId = associatedInfo["range_id"] as! String
                let sourceLocationId = associatedInfo["source_location_id"] as! String
                let sourceCustomerId = associatedInfo["source_customer_id"] as! String
                let sourceAccountNumber = associatedInfo["source_account_number"] as! String
                let requiredPoRef = associatedInfo["requires_po_ref"] as! Bool
        
                let context: [String : Any] = ["auth_token" : token, "organisation_id" : organisationId, "source_id" : sourceId, "range_id" : rangeId, "source_location_id" : sourceLocationId, "source_customer_id" : sourceCustomerId, "source_account_number" : sourceAccountNumber, "requires_po_ref" : requiredPoRef]
                params["context"] = context
            }
            // // // // //
        }
        
        // Creates the JSON object and inserts it into the HTTP body
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch {
            return nil
        }
        
    }
    
    
    // MARK: Methods
    
    /// Creates an HTTP Request and sends it to the server, then calls a handler upon completion.
    ///
    /// - Parameters:
    ///     - callback: Code to execute when the load request is complete.
    ///     - onSuccess: Code to execute in case of success request.
    ///     - onFailure: Code to execute in case of fail request.
    ///     - onConnectionError: Code to execute in case of any connection error with the server.
    func taskWithRequest(onSuccess: @escaping (_ data: Any?) -> Void,
        onFailure: @escaping () -> Void,
        onConnectionError: @escaping (_ message: String) -> Void) {
        
        let task = session.dataTask(with: self.request as URLRequest, completionHandler: {
            data, response, error in
            
            /*print("****** response = \(String(describing: response))")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(String(describing: responseString))")*/

            guard error == nil else {
                
                if !self.isConnectedToNetwork() {
                    onConnectionError(Consts.ERROR_UNREACHABLE_NETWORK)
                } else {
                    onConnectionError(Consts.ERROR_CONNECTION_SERVER)
                }
                
                return
            }
            
            // Parses the response
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                
                // Check status code of the response
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        onFailure()
                    
                        return
                    }
                }
                
                onSuccess(json)
            } catch {
                onConnectionError(Consts.ERROR_CONNECTION_SERVER)
            }
        }) 
        
        task.resume()
        
    }
    
    
    // MARK: Private Methods
    
    // Check for internet connection
    fileprivate func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
        
    }
    
}
