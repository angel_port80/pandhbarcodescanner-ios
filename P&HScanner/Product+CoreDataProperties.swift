//
//  Product+CoreDataProperties.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 27/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Product {

    @NSManaged var barcode: String?
    @NSManaged var name: String?
    @NSManaged var photo: String?
    @NSManaged var lastUpdate: Date?
    @NSManaged var lists: NSSet?

}
