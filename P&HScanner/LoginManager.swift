//
//  LoginManager.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 18/11/2015.
//  Copyright © 2015 Port80. All rights reserved.
//

import Foundation

/// Manages every possible authentication process in different platforms.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
public class LoginManager {
    
    // MARK: Properties
    
    /// Static instance of the class.
    static let sharedInstance = LoginManager()
    
    
    // MARK: Inits
    
    /// Private initializer so nobody can instantiate the class.
    private init() {
        
    }
    
    
    // MARK: Methods
    
    /// Authenticates into Hubtill platform with the username and password given. Once it is authenticated, gets the Authorization Token.
    ///
    /// - Parameters:
    ///     - username: Username of the user to authenticate.
    ///     - password: Password of the user to authenticate.
    ///     - onSuccess: Code to execute in case of success authentication.
    ///     - onFailure: Code to execute in case of fail authentication.
    ///     - onConnectionError: Code to execute in case of any connection error with the server.
    func login(username: String, password: String,
        onSuccess: @escaping (_ data: Any?) -> Void,
        onFailure: @escaping () -> Void,
        onConnectionError: @escaping (_ message: String) -> Void) {
        
        // TODO: this is temporal, until the methods does not need organisaton info in the context
        AuthToken.sharedInstance.id = nil
        // // // //
        
        // Creates HTTP request
        let params = ["method" : "login", "username" : username, "password" : password]
        let loginRequest = RequestHTTP(url: Consts.SINGLE_SIGN_ENDPOINT, operation: .post, params: params)
        
        if let loginRequest = loginRequest {
            loginRequest.taskWithRequest(onSuccess: onSuccess, onFailure: onFailure, onConnectionError: onConnectionError)
        } else {
            onConnectionError(Consts.ERROR_REQUEST_CREATE)
        }
        
    }
    
    /// Logs out. Sets nil to authorized User and Token id and log outs from the server as well.
    ///
    /// - Parameters:
    ///     - onSuccess: Code to execute in case of logout successfully.
    ///     - onFailure: Code to execute in case of logout unsuccessfully.
    ///     - onConnectionError: Code to execute in case of any connection error with the server.
    func logout(tokens: [String],
        onSuccess: @escaping (_ data: Any?) -> Void,
        onFailure: @escaping () -> Void,
        onConnectionError: @escaping (_ message: String) -> Void) {
        
        // Creates HTTP request
        var params: [String : Any] = ["method" : "logout", "auth_tokens" : tokens]
        
        // TODO: this is temporal, until the methods does not need organisaton info in the context
        let context: [String : Any] = ["auth_token" : AuthToken.sharedInstance.id!]
        params["context"] = context
        // // // // //
        
        let logoutRequest = RequestHTTP(url: Consts.SINGLE_SIGN_ENDPOINT, operation: .post, params: params)
        
        if let logoutRequest = logoutRequest {
            logoutRequest.taskWithRequest(onSuccess: onSuccess, onFailure: onFailure, onConnectionError: onConnectionError)
        } else {
            onConnectionError(Consts.ERROR_REQUEST_CREATE)
        }
        
    }
    
}
