//
//  TextInputTableViewCell.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 07/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import UIKit

/// Manages rows in Text Input table view.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class TextInputTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var textField: UITextField!
    
    
    // MARK: Methods
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)

    }

}
