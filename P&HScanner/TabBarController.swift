//
//  TabBarController.swift
//  Scanner
//
//  Created by Ángel David Macho Esperilla on 05/01/2016.
//  Copyright © 2016 Port80. All rights reserved.
//

import UIKit

/// Manages Tab Bar Controller.
///
/// - author: Angel David Macho Esperilla
///
/// - since: version 1.0
class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    // MARK: Properties
    
    /// Indicates if the list is empty. All ViewControllers which belong to this UITabBarController can access to this property.
    var listIsEmpty = true
    
    /// Button which launch barcode scanner
    let buttonScanner = UIButton(type: .custom)
    
    
    // MARK: Methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let selector = #selector(TabBarController.tabToMyList(_:))
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: "tabToMyList"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // Sets a transparent button on Scanner Tab. In this way when you press the Scanner Tab, instead of go tu this item, it create a segue to the camera screen.
        var tabBarButtonSubViews = [UIView]()
        for tabBarButtonSubView in self.tabBar.subviews {
            if tabBarButtonSubView.isKind(of: NSClassFromString("UITabBarButton")!) {
                tabBarButtonSubViews.append(tabBarButtonSubView)
            }
        }
        
        let scannerTabBarButton = tabBarButtonSubViews[1]
        
        self.buttonScanner.frame = CGRect(x: 0.0, y: 0.0, width: scannerTabBarButton.frame.width, height: scannerTabBarButton.frame.height);
        let scannerAction = #selector(TabBarController.showCamera)
        self.buttonScanner.addTarget(self, action: scannerAction, for: .touchUpInside)
        
        self.buttonScanner.frame.origin.x = self.tabBar.frame.origin.x + scannerTabBarButton.frame.origin.x
        self.buttonScanner.frame.origin.y = self.tabBar.frame.origin.y
        
        self.view.addSubview(self.buttonScanner)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        // This code is executed after the screen has rotated
        coordinator.animate(alongsideTransition: nil) {
            _ -> Void in
            
                // When the device rotates, it reallocates the button on the Scanner Tab again
                var tabBarButtonSubViews = [UIView]()
                for tabBarButtonSubView in self.tabBar.subviews {
                    if tabBarButtonSubView.isKind(of: NSClassFromString("UITabBarButton")!) {
                        tabBarButtonSubViews.append(tabBarButtonSubView)
                    }
                }
                
                let scannerTabBarButton = tabBarButtonSubViews[1]
            
                self.buttonScanner.frame.origin.x = self.tabBar.frame.origin.x + scannerTabBarButton.frame.origin.x
                self.buttonScanner.frame.origin.y = self.tabBar.frame.origin.y
            
        }
    }
    
    // Presents the camera screen
    func showCamera() {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let scanner = mainStoryboard.instantiateViewController(withIdentifier: "Scanner") as! ScannerViewController
        
        self.present(scanner, animated: true, completion: nil)
        
    }
    
    /// Change tab selected to "Mylist tab"
    func tabToMyList(_ notification: Notification) {
        
        DispatchQueue.main.async(execute: {
            self.selectedIndex = 0
        })
        
    }
  
}
